<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendEmailOrder;
use Illuminate\Support\Facades\Mail;
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;


ParseClient::initialize('_8Z6tG`B{s^NzjJv', null, '*BcdCwUqN}jqD@t,LF7p?8~NzYGCDfH7"y,<DL=');
ParseClient::setServerURL('http://ione-app-api.ioneservice.com', 'parse');
class MailController extends Controller
{
    public function sendEmail(Request $request) {

        $firstName = $request->get('firstName');
        $lastName = $request->get('lastName');
        $fullName = $firstName. ' '. $lastName;
        $email = $request->get('email');
        $phone = $request->get('phone');
        $pickup = $request->get('pickup');
        $payway = $request->get('payway');
        $orderNote = $request->get('orderNote');

        $pro = $request->get('product');
        $products = json_decode($pro);

//        dump($firstName);
//        dump($lastName);
//        dump($fullName);
//        dump($email);
//        dump($phone);
//        dump($pickup);
//        dump($payway);
//        dump($orderNote);
//        dump($products);
//
//        exit();

        //Save User Order 1 time
        $totalPriceOrder = 0;
        foreach($products as $product){
            $totalPriceOrder += $product->subPrice;
        }
        $userOrder = new UserOrder();
        $userOrder->lastName = $firstName;
        $userOrder->firstName = $lastName;
        $userOrder->pickup = $pickup;
        $userOrder->payway = $payway;
        $userOrder->phone = $phone;
        if($orderNote) {
            $userOrder->orderNote = $orderNote;
        }
        $userOrder->total = $totalPriceOrder;
        $this->saveUserOrder($userOrder);



        $productStock = array();
        //Save user order detail 1 order contains many orderDetail
        foreach($products as $product) {
            $userOrder->productId = $product->productId;
            $userOrder->price = $product->subPrice;
            $userOrder->qty = $product->qty;

            $color = $product->color;
            if($color) {
                $userOrder->color = $color;
            }
//            $size = $product->size;
//            if($size) {
//                $userOrder->size = $size;
//            }
            $this->saveUserOrderDetail($userOrder);

            array_push($productStock, array(
                'productId'=>$product->productId,
                'qty'=>$product->qty
            ));


        }

        $data = array('name' => 'iOne',
            'fullName'=>$fullName,
            'email'=>$email,
            'firstName'=>$firstName,
            'lastName'=>$lastName,
            'phone'=>$phone,
            'pickup'=>$pickup,
            'payway'=>$payway,
            'orderNote'=>$orderNote,
            'products'=>$products,
            'orderId'=>strtoupper($userOrder->orderId)
        );

        Mail::send(['html'=>'pages.mail1'], $data, function($message) use ($data) {
            $message->to($data['email'], $data['fullName'])->subject('Booking order');
            $message->bcc('ioneappservice@gmail.com', 'iOne');
            $message->from('no_reply@ione2u.com', 'iOne');
        });

//        Mail::send(['html'=>'pages.mail1'], $data, function($message) use ($data) {
//            $message->to('no_reply@ione2u.com', $data['fullName'])->subject('Booking order');
//            $message->from('no_reply@ione2u.com', 'iOne');
//        });


        $userCart = new UserCart();
        foreach($productStock as $pro) {
            $userCart->productId = $pro["productId"];
            $userCart->productQty = $pro["qty"];
            $this->cuttingStock($userCart);
        }

        return redirect('success');
    }

    public function saveUserOrder(UserOrder $userOrder) {
        $order = new ParseObject('UserOrder');
        $order->set('firstName', $userOrder->firstName);
        $order->set('lastName', $userOrder->lastName);
        $order->set('total', $userOrder->total);
        $order->set('shipping', 'pickup');
        $order->set('status', 'Ordered');
        $order->set('paymethod', $userOrder->payway);
        $order->set('phone', $userOrder->phone);
        $order->set('pickup', $userOrder->pickup);
        if($userOrder->orderNote) {
            $order->set('note', $userOrder->orderNote);
        }
        $order->save(true);
        $userOrder->orderId = $order->getObjectId();

    }

    public function saveUserOrderDetail(UserOrder $userOrder) {

        $order = new ParseObject("UserOrder", $userOrder->orderId);
        $product = new ParseObject("Product",$userOrder->productId);

        $orderDetail = new ParseObject("UserOrderDetail");
        $size = $userOrder->size;
        if($size) {
            $orderDetail->set('size', $size);
        }
        $color = $userOrder->color;
        if($color) {
            $orderDetail->set('color', $color);
        }
        $orderDetail->set('price', $userOrder->price);
        $orderDetail->set('qty', $userOrder->qty);

        $orderDetail->set('userOrder', $order);
        $orderDetail->set('product', $product);

        $orderDetail->save(true);

    }

    public function cuttingStock(UserCart $userCart) {
        $query = new ParseQuery('Product');
        $query->equalTo('objectId', $userCart->productId);
        $result = $query->first();
        $available = $result->get('available');
        $available = $available - $userCart->productQty;

        $product = new ParseObject('Product', $userCart->productId);
        $product->set('available', $available);
        $product->save(true);
    }

}

class UserOrder {
    public $orderId;
    public $userId;
    public $firstName;
    public $lastName;
    public $total;
    public $pickup;
    public $payway;
    public $phone;
    public $orderNote;
    public $productId;
    public $price;
    public $color;
    public $size;
    public $qty;
}

class UserCart {
    public $cartId;
    public $productId;
    public $productQty;
}
