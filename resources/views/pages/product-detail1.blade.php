@extends('partials.layout')
@section('title', 'Product')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div id="product-img" class="pt-lg-5">
            </div>
            <hr>
            <div id="img-list">

            </div>
        </div>
        <div class="col-md-6">
            <p id="product-name" class="mt-lg-5"></p>
            <p id="product-price" hidden></p>   {{--will be available later, just remove hidden attribute --}}
            <hr>
{{--            <p class="font-weight-light">The iPhone 6 Case features and ultra-thin design, clings to the iPhone 6 and bring a pleasant fell when typing.</p>--}}
            <div>
{{--                <div class="btn-group" style="height: 38px;">--}}
{{--                    <span class="input-group">--}}
{{--                        <button type="button" class="btn-cart-qty" id="btn-minus" style="border-bottom-left-radius: 5px; border-top-left-radius: 5px;"><i class="fa fa-minus"></i></button>--}}
{{--                    </span>--}}
{{--                    <input style="width: 50px;" value="1" disabled class="text-center" id="qty">--}}
{{--                    <span class="input-group">--}}
{{--                        <button type="button" class="btn-cart-qty" id="btn-plus" style="border-bottom-right-radius: 5px; border-top-right-radius: 5px;"><i class="fa fa-plus"></i></button>--}}
{{--                    </span>--}}
{{--                </div>--}}
                <button class="btn-add-2-cart font-weight-light" id="btn-add-2-cart">Order now</button>
            </div>
            <p title="Unavailable!" data-toggle="popover" data-trigger="focus"  id="item-available"></p>

            <div id="product-color">
                <p class="font-weight-light">COLOR</p>
                <div style="display: inline;" class="select-color"></div>
                <input type="hidden" id="final-selected-color">
            </div>
            <div id="product-size" style="margin-top: 20px;">
                <p class="font-weight-light">SIZE</p>
                <div style="display: inline;" class="select-size"></div>
                <input type="hidden" id="final-selected-size">
            </div>
            <script type="text/javascript" src="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.6/src/jquery.ez-plus.js"></script>
            <p class="font-weight-light mt-3">Categories: App Accessories, Apple Accessories, Bag & Sleeves, Cables & Adapters, Cases & Protections, Others, Power Bank, Speaker & Headphones</p>
        </div>
    </div>
    <hr class="mt-lg-3">
    <div class="row">
        <ul style="list-style-type: none; padding-top: 10px;" id="desc-review">
            <li style="float: left; cursor: pointer; margin-right: 20px;" id="desc">Description</li>
            <li style="float: left; cursor: pointer; margin-left: 30px;" id="review">Reviews(0)</li>
        </ul>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <h6 class="pl-4" id="pro-desc-title">Product Description</h6>
            <div id="pro-desc"></div>

            <h6 class="pl-4" id="pro-review-title">Product Review</h6>
            <div id="pro-review">
                <p class="font-weight-light pl-4">There is no review yet.</p>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Login required!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-card-tab" data-toggle="tab" href="#nav-card" role="tab" aria-controls="nav-card" aria-selected="false" style="color: black;">Login with iOneCard</a>
                            <a class="nav-item nav-link" id="nav-phone-tab" data-toggle="tab" href="#nav-phone" role="tab" aria-controls="nav-phone" aria-selected="true" style="color: black;">Login with phone</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-card" role="tabpanel" aria-labelledby="nav-card-tab">
                            <form>
                                <div class="form-group row mt-4">
                                    <label for="disabledInput" class="col-sm-3 control-label">iOneCard Number :</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm mb-3">
                                            <input type="text" class="rounded no-border-focus" id="card-number" placeholder="XXXX-XXXX-XXXX-XXXX" style="width: 300px; border: 1px solid #dedede; padding-left: 5px;">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="nav-phone" role="tabpanel" aria-labelledby="nav-phone-tab">
                            <form>
                                <div class="form-group row mt-4">
                                    <label for="disabledInput" class="col-sm-3 control-label">Phone Number :</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-sm">+855</span>
                                            </div>
                                            <input type="text" class="rounded-right no-border-focus" id="phone-number" placeholder="12345678" maxlength="10" style="width: 250px; border: 1px solid #dedede;">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row ">
                            <p class="text-center mx-auto">Need a iOneCard account?
                                <a href="https://play.google.com/store/apps/details?id=com.ionecardapp" target="_blank" class="android-link"><u>Create an account</u></a>
                            </p>
                        </div>
                        <div class="row">
                            <p class="text-center mx-auto font-weight-light" style="font-size: 12px;">For IOS mobile, Please call us for register 023 99 61 72</p>
                        </div>
                        <div class="row">
                            <div class="mx-auto">
                                <a href="https://apps.apple.com/kh/app/ione-card/id1225145703" target="_blank">
                                    <img src="{{url('images/ione-app-store.png')}}" class="img-fluid" style="width: 160px; height: 200px; padding-right: 5px;">
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.ionecardapp" target="_blank">
                                    <img src="{{url('images/ionecard-android.png')}}" class="img-fluid" style="width: 160px; height: 200px; padding-left: 5px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn" style="background-color: #1cbac8; color: white;" id="btn-login" data-toggle="modal">Login</button>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-lg-5"></div>
@endsection
@section('product-api')
    <script>

        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function(m,key,value) {
                    vars[key] = value;
                });
            return vars;
        }
        var productId = getUrlVars()["productId"];


        let request = {
            productId: productId
        }
        $.ajax({
            type: 'POST',
            url: apiUrl('product_detail'),
            data: request,
            headers:{
                'X-Parse-Application-Id': apiAppId()
            },
            async: false
        }).done(function (res) {

            //product image
            let result = res.result[0];
            let productImg = '<img src="'+result.images[0]+'" data-zoom-image="'+result.images[0]+'" id="pro-img"/>';
            $('#product-img').append(productImg);


            let imgList = result.images;
            $.each(imgList, function (index, item) {
                let img = '<div style="display: inline;" class="singleImg">' +
                    '           <a data-image="'+item+'" data-zoom-image="'+item+'" >' +
                    '               <img src="'+item+'" style="width: 80px; height: 80px; border: 1px solid lightgrey; margin: 2px; cursor: pointer;" id="img'+index+'"/>'+
                    '           <a/>' +
                    '       </div>';
                $('#img-list').append(img);
            });



            var zoomConfig = {scrollZoom: true};

            var zoomImage = $('img#pro-img');
            zoomImage.ezPlus(zoomConfig);

            var image =$('.singleImg a');
            $('#img0').css('border', '2px solid #1cbac8');
            image.on('click', function (e) {


                $('.singleImg a img').css('border', '1px solid lightgrey');
                $(e.target).css('border', '2px solid #1cbac8');

                $('.product-img').remove();
                zoomImage.removeData('elevateZoom');

                zoomImage.attr('src', $(this).data('image'));
                zoomImage.data('zoom-image', $(this).data('zoom-image'));

                zoomImage.ezPlus(zoomConfig);

            })



            //product info
            let usd = '$ ';
            $('#product-name').html('<p style="font-size: 30px;" class="font-weight-light">'+result.name+'</p>');
            $('#product-price').html('<b style="font-size: 30px;">'+usd+result.price.toFixed(2)+'</b>');


            var availableItem = '';
            if(result.available>1){
                availableItem = ' items available';
            } else {
                availableItem = ' item available';
            }
            $('#item-available').html('<p class="font-italic font-weight-light">'+result.available+ availableItem+'</p>');




            //working on color
            $('#product-color').hide();
            if(result.color) {
                if (result.color.length>0) {
                    $('#product-color').show();
                    for(let i = 0; i < result.color.length; i++){
                        let color = '<div class="font-weight-light product-color" id="color'+i+'">'+result.color[i]+'</div>';
                        $('.select-color').append(color);
                    }

                }
                // if(result.color.length > 0) {
                //     for(let i = 1; i <= result.color.length; i ++) {
                //         $('div').find("#color"+i).css('margin-left', '20px');
                //     }
                // }

                var finalColor = $('#color0').text();
                $('#color0').css('border', '2px solid #1cbac8');
                $('.product-color').on('click', function (e) {
                    $('.select-color .product-color').css('border', '1px solid lightgrey');
                    $(e.target).css('border', '2px solid #1cbac8');
                    $('#final-selected-color').val($(this).text());
                    finalColor = $('#final-selected-color').val();
                });
            }


            //working on size
            $('#product-size').hide();
            if(result.size) {
                if (result.size.length>0) {
                    $('#product-size').show();
                    for(let i = 0; i < result.size.length; i++){
                        let color = '<div class="font-weight-light product-size" id="size'+i+'">'+result.size[i]+'</div>';
                        $('.select-size').append(color);
                    }

                }
                // if(result.size.length > 0) {
                //     for(let i = 1; i <= result.size.length; i ++) {
                //         $('div').find("#size"+i).css('margin-left', '20px');
                //     }
                // }

                var finalSize = $('#size0').text();
                $('#size0').css('border', '2px solid #1cbac8');
                $('.product-size').on('click', function (e) {
                    $('.select-size .product-size').css('border', '1px solid lightgrey');
                    $(e.target).css('border', '2px solid #1cbac8');
                    $('#final-selected-size').val($(this).text());
                    finalSize = $('#final-selected-size').val();
                });
            }


            function toggleAlert() {
                let alert = '<div class="alert alert-success alert-dismissible fade " role="alert" id="alertSuccessAddCart" style="display: none;">' +
                    '            <strong>'+result.name+'</strong> has been added to cart. <a href="/cart" style="color: #1cbac8;">View cart</a>' +
                    '            <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '                <span aria-hidden="true">&times;</span>' +
                    '            </button>' +
                    '        </div>'
                $('#alertContainer').append(alert);


                $('#alertSuccessAddCart').addClass('show');
                $('#alertSuccessAddCart').css('display', 'block');

                $('#alertSuccessAddCart').on('closed.bs.alert', function () {
                    let alert = '<div class="alert alert-success alert-dismissible fade " role="alert" id="alertSuccessAddCart" style="display: none;">' +
                        '            <strong>'+result.name+'</strong> has been added to cart. <a href="/cart" style="color: #1cbac8;">View cart</a>' +
                        '            <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '                <span aria-hidden="true">&times;</span>' +
                        '            </button>' +
                        '        </div>'
                    $('#alertContainer').append(alert);
                })
            }

            $('#btn-add-2-cart').click(function () {
                //localStorage.clear();


                var userCart;
                var productId = '';

                userCart = JSON.parse(localStorage.getItem('userCart'));
                if(userCart == null) {
                    userCart = [];
                }

                let cart = {
                    productId: result.objectId,
                    proName: result.name,
                    proImg: result.images[0],
                    proAvailable: result.available,
                    qty: 1,
                    color: finalColor,
                    size: finalSize,
                    unitPrice: result.price,
                    subPrice: result.price,
                }

                //get product id
                if(userCart != null){
                    let product = userCart.filter(data => data.productId == result.objectId);
                    if(product.length > 0) {
                        productId = product[0].productId;
                    }
                }

                //update qty and price only coz product already in cart
                if(productId == result.objectId) {
                    /*let index = userCart.findIndex(obj => obj.productId == productId);
                    if(userCart[index].qty <= result.available) {
                        userCart[index].qty += 1;
                        userCart[index].subPrice = userCart[index].qty * result.price;

                        if(userCart[index].qty > result.available) {
                            $('#item-available').attr('data-placement','left')
                            $('#item-available').attr('data-content', 'Please check and see available item for this product.');
                            $('#item-available').popover('show');
                            setTimeout(function () {
                                $('#item-available').popover('hide');
                            }, 2000);

                            userCart[index].qty = result.available;
                            userCart[index].subPrice = userCart[index].qty * result.price;
                        } else {
                            toggleAlert();
                        }
                        localStorage.setItem('userCart', JSON.stringify(userCart));
                        $('#cart-badge').show();
                        let totalCartCount = 0;
                        let newUserCart = JSON.parse(localStorage.getItem('userCart'));
                        $.each(newUserCart, function (index, item) {
                            totalCartCount += item.qty;
                        });
                        $('#cart-count').text(totalCartCount);


                    } */
                    $('#product-name').attr('data-placement','right')
                    $('#product-name').attr('data-content', 'This product already in cart. All products allows 1 quantity only for 1 order.');
                    $('#product-name').popover('show');
                    setTimeout(function () {
                        $('#product-name').popover('hide');
                    }, 2200);
                } else { // add new product to cart
                    userCart.push(cart);
                    localStorage.setItem('userCart', JSON.stringify(userCart));
                    $('#cart-badge').show();
                    let totalCartCount = 0;
                    let userCartCount = JSON.parse(localStorage.getItem('userCart'));
                    if(userCartCount != null){
                        $.each(userCartCount, function (index, item) {
                            totalCartCount += item.qty;
                        })
                        $('#cart-count').text(totalCartCount);
                        toggleAlert();
                    }else {
                        $('#cart-count').text(1);
                        toggleAlert();
                    }

                }
                console.log(userCart);







                        // localStorage.setItem('isBadgeVal', true);
                        // Swal.fire({
                        //     title: 'Success!',
                        //     ///text: result.name+' has been add to your cart.',
                        //     html:
                        //         '<i style="color:#1cbac8">'+result.name+'</i>' +
                        //         '<p>has been added to cart.</p>',
                        //     type: 'success',
                        //     confirmButtonText: 'Ok',
                        //     confirmButtonColor: '#1cbac8'
                        // }).then(function () {
                        //
                        //     $('#cart-badge').show();
                        //     let cartVal = $('#cart-count').text();
                        //     cartVal = parseInt(cartVal) + parseInt($('#qty').val());
                        //     $('#cart-count').text(cartVal);
                        //
                        // });


                        // $.ajax({
                        //     type: 'POST',
                        //     url: apiUrl('add_cart'),
                        //     data: "{\"userId\": \""+userId+"\", \"productId\": \""+result.objectId+"\",  \"size\": \""+''+"\",  \"color\": \""+''+"\", \"qty\": "+parseInt($('#qty').val())+", \"price\": "+parseFloat(result.price)*parseInt($('#qty').val())+"}",
                        //     headers: {
                        //         'X-Parse-Application-Id': apiAppId()
                        //     },
                        //     contentType: 'application/json',
                        //     async: false
                        // }).done(function (res) {
                        //     localStorage.setItem('isBadgeVal', true);
                        //     console.log(res.result);
                        //     Swal.fire({
                        //         title: 'Success!',
                        //         //text: result.name+' has been add to your cart.',
                        //         html:
                        //             '<i style="color:#1cbac8">'+result.name+'</i>' +
                        //             '<p>has been added to cart.</p>',
                        //         type: 'success',
                        //         confirmButtonText: 'Ok',
                        //         confirmButtonColor: '#1cbac8'
                        //     }).then(function () {
                        //
                        //         $('#cart-badge').show();
                        //         let cartVal = $('#cart-count').text();
                        //         cartVal = parseInt(cartVal) + parseInt($('#qty').val());
                        //         $('#cart-count').text(cartVal);
                        //
                        //     });
                        // }).catch(function (error) {
                        //     console.log(error);
                        // });


            });




            $('#desc').css('color', '#1cbac8');
            $('#desc-review').on('click', function (e) {
                $('#desc-review li').css('color', '#212529');
                $(e.target).css('color', '#1cbac8');
            });

            let desc = '<p class="font-weight-light pl-4">'+result.desc+'</p>';
            $('#pro-desc').append(desc);

            $('#pro-review-title').hide();
            $('#pro-review').hide();


            $('#review').click(function () {
                $('#pro-review-title').show();
                $('#pro-review').show();

                $('#pro-desc-title').hide();
                $('#pro-desc').hide();
            });
            $('#desc').click(function () {
                $('#pro-desc-title').show();
                $('#pro-desc').show();

                $('#pro-review-title').hide();
                $('#pro-review').hide();
            })


        });


    </script>
@endsection
