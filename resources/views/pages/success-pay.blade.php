@extends('partials.layout')
@section('title', 'Shipping')

@section('content')
    <script>
        localStorage.clear();
        let userCart = JSON.parse(localStorage.getItem('userCart'));
        if(userCart.length < 1) {
            $('#cart-badge').hide();
        }
    </script>
    <div class="row font-weight-light" style="padding-bottom: 50px;">
        <script>
            localStorage.removeItem('isBadgeVal');
        </script>
        <div class="col-md-3"></div>
        <div class="col-md-6" style="text-align: center;padding-top: 50px; color: #545b62;">
            <img src="{{url('images/success.png')}}" style="width: 20%;"/>
            <div style="font-size: 13px;">
                <p  style="font-size: 30px;" >Thank for your booking order</p>
                <p style="width: 100%;" class="font-weight-light">Pick Up Address: Nº. 25, Mao Tse Tung Blvd., 1st Floor, iOne Building,<br>
                    Boeung Keng Kang 1, Chamkar Morn, 12302<br>
                    Phnom Penh, Cambodia.
                </p>
            </div>

            <p style="font-weight: bold"> More information: 023 99 61 72 </p>
            <p style="width: 100%; font-size: 13px;" class="font-weight-light">Please come to any of our retail within 48 hours to make booking down payment to source your booking order.

            </p>
        </div>
        <div class="col-md-3"></div>
    </div>

@endsection
