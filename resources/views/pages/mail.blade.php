<div style="width: 40%;">

    We want to confirm your order has been succeeded.

    <h4>Your Info</h4><hr>
    <p>First Name : {{$firstName}}</p>
    <p>Last Name : {{$lastName}}</p>
    <p>Phone Number : {{$phone}}</p>


    <h4>Your Order</h4><hr>
    <h5>OrderID: {{$orderId}}</h5>
    <table style="border-collapse: collapse; width: 100%;">
        <thead>
        <tr>
            <th style="padding:8px; text-align: left; border-bottom: 1px solid #ddd;">#</th>
            <th style="padding:8px; text-align: left; border-bottom: 1px solid #ddd;">Product Name</th>
            <th style="padding:8px; text-align: left; border-bottom: 1px solid #ddd;">Qty</th>
            <th style="padding:8px; text-align: left; border-bottom: 1px solid #ddd; text-align: center;" width="100">Price</th>
            <th style="padding:8px; text-align: left; border-bottom: 1px solid #ddd; text-align: center;" width="100">Subtotal</th>
        </tr>
        </thead>
        <tbody>
        <?php $total=0; ?>
        @foreach($products as $index=>$product)
            <tr>
                <td style="padding:8px; text-align: left; border-bottom: 1px solid #ddd;">{{$index+1}}</td>
                <td style="padding:8px; text-align: left; border-bottom: 1px solid #ddd;">{{$product->product->name}}</td>
                <td style="padding:8px; text-align: left; border-bottom: 1px solid #ddd;">{{$product->qty}}</td>
                <td style="padding:8px; border-bottom: 1px solid #ddd; text-align: center;" width="100">{{'$ '.$product->product->price}}</td>
                <td style="padding:8px; border-bottom: 1px solid #ddd; text-align: center;" width="100">{{'$ '.$product->price}}</td>
            </tr>
            <?php $total += $product->price;?>
        @endforeach
        <tr>
            <td colspan="3" style="padding:8px; text-align: left; border-bottom: 1px solid #ddd;">Total</td>
            <td colspan="4" style="padding-right:40px; text-align: right; border-bottom: 1px solid #ddd;">$ {{$total}}</td>
        </tr>
        </tbody>
        <tfoot>

        </tfoot>
    </table>
</div>
