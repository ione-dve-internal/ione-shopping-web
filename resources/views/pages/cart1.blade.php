@extends('partials.layout')
@section('title', 'Cart')

@section('content')

    <table  id="customers">
        <thead>
        <tr>
            <th style="text-align: center;">PRODUCT</th>
            <th class="text-center">COLOR</th>
            <th class="text-center">SIZE</th>
            <th class="text-center">QUANTITY</th>
        </tr>
        </thead>
        <tbody class="table-body">

        </tbody>
        <tr style="background-color: whitesmoke;">
            <td></td>
            <td></td>
            <td></td>
            <td>
                <button type="button" class="btn btn-checkout" id="btn-update-cart"  >UPDATE CART</button>
            </td>
        </tr>
    </table>

    <div class=" mb-lg-5 mt-lg-5">
        <div class="row">
            <div class="col-md-8"></div>
{{--            <div class="col-md-4">--}}
{{--                <p>Cart Totals</p>--}}
{{--                <table class="table">--}}
{{--                    <tr>--}}
{{--                        <td style="background-color: whitesmoke;">Subtotal</td>--}}
{{--                        <td style="background-color: #f9f9f9;" class="text-center" id="sub-total-price">$1992</td>--}}
{{--                    </tr>--}}
{{--                    <tr style="border-bottom: 1px solid #dcdcde;">--}}
{{--                        <td style="background-color: whitesmoke;">Total</td>--}}
{{--                        <td style="background-color: #f9f9f9;" class="text-center" id="total-price">$1992</td>--}}
{{--                    </tr>--}}
{{--                </table>--}}
{{--            </div>--}}
        </div>
        <div class="d-flex flex-row-reverse mt-lg-4">
            <button type="button" class="btn btn-checkout"  id="btn-proceed-to-checkout">PROCEED TO CHECKOUT</button>
        </div>
    </div>


@endsection

@section('cart-api')
    <script>
        let userCartCount = JSON.parse(localStorage.getItem('userCart'));
        if (userCartCount != null) {
            $.each(userCartCount, function (index, item) {
                let usd = '$ ';
                if(typeof item.color === 'undefined') {item.color = ''}
                if(typeof item.size === 'undefined') {item.size = ''}


                let tbody = '<tr class="cart-table-row" product-id="'+item.productId+'"  product-color="'+item.color+'" product-size="'+item.size+'"  id="cart-table-row'+index+'">' +
                    '                <td>' +
                    '                    <i class="fa fa-times remove-cart-icon" aria-hidden="true" style=""></i>' +
                    '                    <img src="'+item.proImg+'" style="height: 200px; width:200px; padding-left: 10px;">' +
                    '                    <label style="color: #1cbac8;padding-left: 20px; width: 390px; cursor: pointer;" class="product-name">'+item.proName+'</label>' +
                    '                    <input type="hidden" value="'+item.productId+'" class="product-id"/>'+
                    '                </td>' +
                    // '                <td class="pro-price">'+usd+item.unitPrice.toFixed(2)+'</td>' +
                    // '                <td>' +
                    // '                    <div class="cart-row">' +
                    // '                        <div class="btn-group" style="height: 38px;">' +
                    // '                        <span class="input-group">' +
                    // '                            <button type="button" class="btn-cart-qty btn-minus" id="btn-minus" style="border-bottom-left-radius: 5px; border-top-left-radius: 5px;"><i class="fa fa-minus"></i></button>' +
                    // '                        </span>' +
                    // '                            <input style="width: 50px;" value="'+item.qty+'" disabled class="text-center qty-val" id="qty">' +
                    // '                            <input value="'+item.proAvailable+'" type="hidden" class="product-available"/>' +
                    // '                            <span class="input-group">' +
                    // '                            <button type="button" class="btn-cart-qty btn-plus" id="btn-plus" style="border-bottom-right-radius: 5px; border-top-right-radius: 5px;"><i class="fa fa-plus"></i></button>' +
                    // '                        </span>' +
                    // '                        </div>' +
                    // '                    </div>' +
                    //    '                <td class="text-center"></td><td class="text-center" id="pro-size"></td>'+
                    // '                </td>' +
                    // '                <td class="text-center pro-total-price">'+usd+(item.subPrice).toFixed(2)+'</td>' +
                       '<td class="text-center">'+item.color+'<td class="text-center">'+item.size+'<td class="text-center">1</td>'+
                    '            </tr>';


                $('.table-body').append(tbody);
            });



            $('body').on('click', '.remove-cart-icon', function () {

                let parent = $(this).parent().parent();
                parent.remove();

                let cartCount = $('#cart-count');
                let val = parseInt(cartCount.text());
                val = val - 1;
                cartCount.text(val);

                if (val == 0) {
                    $('#cart-badge').hide();
                }
                let productId = parent.attr('product-id');
                let index = userCartCount.findIndex(obj => obj.productId == productId);
                userCartCount.splice(index, 1);

               /* let parent = $(this).parent().parent();
                parent.remove();
                let qty = parent.find('.qty-val').val();
                let cartCount = $('#cart-count');
                let cartVal = cartCount.text();
                let newCartCount = cartVal - qty;
                cartCount.text(newCartCount);

                // subtotal price
                let proTotalPrice = parent.find('.pro-total-price').text();
                proTotalPrice = proTotalPrice.substring(1);
                proTotalPrice = parseFloat(proTotalPrice);
                let subTotalPrice = $('#sub-total-price').text();
                subTotalPrice = subTotalPrice.substring(1);
                subTotalPrice = parseFloat(subTotalPrice);
                let newSubtotalPrice = subTotalPrice - proTotalPrice;
                $('#sub-total-price').text('$ '+newSubtotalPrice.toFixed(2));
                $('#total-price').text('$ '+newSubtotalPrice.toFixed(2));



                let productId = parent.attr('product-id');
                let index = userCartCount.findIndex(obj => obj.productId == productId);
                userCartCount.splice(index, 1);

                */
            });


            $('body').on('click', '.btn-minus', function () {
                isUpdateCart = false;
                let parent = $(this).parent().parent();
                let tableRow = parent.parent().parent().parent();


                let qty = parent.find('.qty-val').val();
                qty --;
                if(qty >= 1){
                    let cartCount = $('#cart-count');
                    let cartVal = cartCount.text();
                    let newCartCount = cartVal - 1;
                    cartCount.text(newCartCount);


                    // update total price
                    let price = tableRow.find('.pro-price').text();
                    price = price.substring(1);
                    price = parseFloat(price);

                    let totalPrice = tableRow.find('.pro-total-price').text();
                    totalPrice = totalPrice.substring(1);
                    totalPrice = parseFloat(totalPrice);

                    let newTotalPrice = totalPrice - price;
                    tableRow.find('.pro-total-price').text('$ ' +newTotalPrice.toFixed(2));

                    let subTotalPrice = $('#sub-total-price').text();
                    subTotalPrice = subTotalPrice.substring(1);
                    subTotalPrice = parseFloat(subTotalPrice);
                    let newSubtotalPrice = subTotalPrice - price;
                    $('#sub-total-price').text('$ '+newSubtotalPrice.toFixed(2));
                    $('#total-price').text('$ '+newSubtotalPrice.toFixed(2));
                }
                if(qty < 1) {
                    qty = 1;
                }
                parent.find('.qty-val').val(qty);

                let productId = tableRow.attr('product-id');
                let index = userCartCount.findIndex(obj => obj.productId == productId);
                userCartCount[index].qty = qty;
                userCartCount[index].subPrice = qty * userCartCount[index].unitPrice;

            });


            $('body').on('click', '.btn-plus', function () {
                let parent = $(this).parent().parent();
                let tableRow = parent.parent().parent().parent();
                let proAvailable = parent.find('.product-available').val();
                let qty = parent.find('.qty-val').val();
                qty ++;



                if(qty <= proAvailable) {
                    let cartCount = $('#cart-count');
                    let cartVal = parseInt(cartCount.text());
                    let newCartCount = cartVal + 1;
                    cartCount.text(newCartCount);


                    // update total price
                    let price = tableRow.find('.pro-price').text();
                    price = price.substring(1);
                    price = parseFloat(price);

                    let totalPrice = tableRow.find('.pro-total-price').text();
                    totalPrice = totalPrice.substring(1);
                    totalPrice = parseFloat(totalPrice);

                    let newTotalPrice = totalPrice + price;
                    tableRow.find('.pro-total-price').text('$ ' +newTotalPrice.toFixed(2));

                    let subTotalPrice = $('#sub-total-price').text();
                    subTotalPrice = subTotalPrice.substring(1);
                    subTotalPrice = parseFloat(subTotalPrice);
                    let newSubtotalPrice = subTotalPrice + price;
                    $('#sub-total-price').text('$ '+newSubtotalPrice.toFixed(2));
                    $('#total-price').text('$ '+newSubtotalPrice.toFixed(2));

                }

                if(qty > proAvailable){
                    qty = proAvailable;
                    if(proAvailable > 1) {
                        Swal.fire({
                            title: 'Invalid!',
                            text: 'This product available only '+proAvailable+ ' items',
                            type: 'warning',
                            confirmButtonText: 'Ok',
                            confirmButtonColor: '#1cbac8',
                        })
                    } else {
                        Swal.fire({
                            title: 'Invalid!',
                            text: 'This product available only 1 item',
                            type: 'warning',
                            confirmButtonText: 'Ok',
                            confirmButtonColor: '#1cbac8',
                        })
                    }

                }
                parent.find('.qty-val').val(qty);

                let productId = tableRow.attr('product-id');
                let index = userCartCount.findIndex(obj => obj.productId == productId);
                userCartCount[index].qty = qty;
                userCartCount[index].subPrice = qty * userCartCount[index].unitPrice;

            });




            $('body').on('click', '.product-name', function () {
                let parent = $(this).parent();
                let productId = parent.find('.product-id').val();
                //console.log(productId);
                location.href="{{url('product-detail?productId=')}}"+productId;
            });


            //total price
            let totalPrice1 = 0;
            let tableBody = $('.table-body').find('.cart-table-row')
            for (let i = 0; i< tableBody.length; i ++){
                let totalPrice = $('#cart-table-row'+i).find('.pro-total-price').text();
                totalPrice = totalPrice.substring(1);
                totalPrice1 += parseFloat(totalPrice);
            }
            $('#sub-total-price').text('$ '+totalPrice1.toFixed(2));
            $('#total-price').text('$ '+totalPrice1.toFixed(2));





            $('body').on('click', '#btn-update-cart', function () {
                //console.log(userCartCount);
                localStorage.setItem('userCart', JSON.stringify(userCartCount));
                $('#alert-cart-update').css('display', 'block')
                setTimeout(function () {
                    $('#alert-cart-update').css('display', 'none')
                }, 2000);
            });



            $('body').on('click', '#btn-proceed-to-checkout', function () {
                location.href="{{url('/shipping')}}";
            });

        } else  {
            location.href="{{url('/products')}}";
        }

    </script>
@endsection
