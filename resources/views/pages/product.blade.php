@extends('partials.layout')
@section('title', 'Products')
@section('active-product-page', 'active-menu-color')
@section('active-apple-product', 'white')
@section('pagination')
@endsection

@section('product-api')
    <script>
        $.ajax({
            type: 'POST',
            url: apiUrl('product_ione_dot_com_web'),
            headers:{
                'X-Parse-Application-Id': apiAppId()
            },
            async: false
        }).done(function (res) {
            var result = res.result;

            let usd = 'USD ';


            $.ajax({
                type: 'POST',
                url: apiUrl('productCategory'),
                headers:{
                    'X-Parse-Application-Id': apiAppId()
                },
                async: false
            }).done(function (res) {
                $('#loading-gif').hide();
                $('#allProCate').text('All');
                $('#firstCateSlash').text('/');
                $('#row').removeAttr('style');
                let slash = '  /';
                let categories = res.result;
                $.each(categories, function (index, category) {
                    let cateHtml = '<li class="category-item" category-id="'+category.objectId+'">'+category.title+'</li>'+
                                    '<li class="category-slash">'+slash+'</li>';
                    $('.pro-category').append(cateHtml);
                });
                $('.category-slash').last().remove();
                $('#allProCate').css('color', '#1cbac8');
                $('.category-item').on('click', function (e) {
                    $('ul .category-item').css('color', '#777777');
                    $(e.target).css('color', '#1cbac8');


                    let cateId = $(this).attr('category-id');

                    let filterData = {
                        keyword: '',
                        filter: cateId
                    }
                    $.ajax({
                        type: 'POST',
                        url: apiUrl('product_search_ione_web'),
                        data: filterData,
                        headers:{
                            'X-Parse-Application-Id': apiAppId()
                        },
                        async: false
                    }).done(function (res) {
                        let products = res.result;
                        $('#show-product').html('');
                        $('#pagination-container').html('');
                        if(products.length>0) {
                            firstRender(products);
                        } else {
                            Swal.fire({
                                title: 'Error',
                                text: 'No product found for this category!',
                                type: 'error'
                            }).then(function () {
                                $('#allProCate').click();
                            });
                        }

                    });

                });
            });

            firstRender(result);
            function firstRender(products) {

                let current_page = 1;
                let record_per_page = 20;
                let paginationContainer = '<ul class="pagination" id="pagination">' +
                    '            <li class="page-item"><a class="btn btn-pagination" href="#" id="btnPrevious">Previous</a></li>' +
                    '        </ul>';
                $('#pagination-container').append(paginationContainer);
                for(let x = 1; x < numOfPage()+1; x++) {
                    let pageList = '<li class="page-item"><a class="page-link" href="#" id="page'+x+'">'+x+'</a></li>';
                    $('#pagination').append(pageList);
                }
                let next = '<li class="page-item" ><a class="btn btn-pagination" href="#" id="btnNext">Next</a></li>';
                $('#pagination-container ul li:last').after(next);





                onPageChange(1);
                $('#page1').addClass('active-pagination');
                $('#page1').focus();

                function onPageChange(page) {
                    if(page < 1){
                        page = 1;
                    }
                    if(page> numOfPage()) page = numOfPage();



                    for (let i = (page - 1) * record_per_page; i < (page * record_per_page) && i < products.length; i++) {

                        let limit_length = 22;
                        let proName = products[i].name.length > limit_length ? products[i].name.substring(0, limit_length - 3) + '...' : products[i].name;
                            let html2 = '<div class="col-md-3 col-sm-6" style="margin-top: 30px;">' +
                            '                    <div class="box8">' +
                            '                        <img src="'+products[i].images[0]+'">' +
                            '                        <h6 class="title" style="font-size: 15px;">'+proName+'</h6>' +
                            '                        <div class="box-content">' +
                            '                            <p style="color: white; text-align: center; padding-top: 20px;">'+products[i].name+'</p>'    +
                            '                            <ul class="icon" data-id="'+products[i].objectId+'">' +
                            '                                <li><a href="#"><i class="fa fa-eye" style="padding-left: 20px; padding-top: 18px;"></i></a> </li>' +
                            '                            </ul>' +
                            '                        </div>' +
                            '                    </div>' +
                            '                </div>';
                        $("#show-product").append(html2);
                    }

                    $('.icon').click(function () {
                        var productId = $(this).attr('data-id');
                        navigateToDetail(productId);
                    });

                }

                function numOfPage() {
                    return Math.ceil(products.length/record_per_page);
                }

                $('#btnPrevious').click(function (e) {
                    if(current_page > 1) {
                        current_page --;
                        $('#show-product').html('');
                        var $activePage=$('.pagination').find("li a.active-pagination");
                        $activePage.removeClass('active-pagination');

                        onPageChange(current_page);
                        $('.page-link:eq('+(current_page-1)+')').addClass('active-pagination');
                        $('.page-link:eq('+(current_page-1)+')').focus();
                    } else {
                        $('#page1').focus();
                    }
                    e.preventDefault();
                });
                $('#btnNext').click(function (e) {
                    if(current_page < numOfPage()) {
                        current_page ++;
                        $('#show-product').html('');
                        var $activePage=$('.pagination').find("li a.active-pagination");
                        $activePage.removeClass('active-pagination');

                        onPageChange(current_page);
                        $('.page-link:eq('+(current_page-1)+')').addClass('active-pagination');
                        $('.page-link:eq('+(current_page-1)+')').focus();
                    } else {
                        $('.page-link:eq('+(current_page-1)+')').focus();
                    }
                    e.preventDefault();
                });

                $('li .page-link').click(function (e) {
                    let pageNumber = $(this).text();
                    //console.log(pageNumber);
                    $('#show-product').html('');
                    var $activePage=$('.pagination').find("li a.active-pagination");
                    $activePage.removeClass('active-pagination');
                    onPageChange(pageNumber);
                    $('.page-link:eq('+(pageNumber-1)+')').addClass('active-pagination');
                    current_page = pageNumber;
                    e.preventDefault();
                });
            }


            function navigateToDetail(productId) {
                location.href="{{url('product-detail?productId=')}}"+productId;
            }


        });



    </script>
@endsection
@section('content')
    <div class="row" style="height: 500px;" id="row">
        <img src="{{url('images/loadingImage.gif')}}" class="mx-auto img-fluid mt-lg-5" id="loading-gif" style="width: 50px; height: 50px;">
        <ul class="pro-category container">
            <li class="category-item" id="allProCate"></li>
            <li class="category-slash" id="firstCateSlash"></li>
        </ul>
    </div>
    <div class="row" id="show-product">

    </div>
    <div class="d-flex justify-content-center mt-lg-5 mb-lg-5" id="pagination-container">

{{--        <ul class="pagination" id="pagination">--}}
{{--            <li class="page-item"><a class="btn btn-pagination" href="#" id="btnPrevious">Previous</a></li>--}}
{{--        </ul>--}}
    </div>

@endsection
