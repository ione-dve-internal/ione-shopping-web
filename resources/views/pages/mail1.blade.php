<div style="width: 100%; background-color: whitesmoke; padding-bottom: 1px;">

    <div style="display: block; padding-left: 42%; margin-bottom: 10px;  padding-top: 10px;">
        <img src="{{url('images/receiptlogo.png')}}" style="width: 100px; ">
    </div>

    <div style="background-color: white; margin-left: 10px; margin-right: 10px; margin-bottom: 10px; padding-left: 10px; padding-top: 10px;">
        <p style="text-align: center;"><strong style=" ">Thank you! We have received your online booking order.</strong></p>
        <p>Please come to any of our retail stores within 48 hrs to make booking down payment to secure your booking order.</p>
        <strong>Your Info</strong><hr style="margin-right: 10px; border: 0; border-top: 1px solid #ccc;">
        <p>First Name : {{$firstName}}</p>
        <p>Last Name : {{$lastName}}</p>
        <p>Phone Number : {{$phone}}</p>


        <strong>Your Order</strong><hr style="margin-right: 10px; border: 0; border-top: 1px solid #ccc;">
        <span style="color: #1cbac8; font-size: 17px; font-weight: bold;">Order Number: </span><span style="font-weight: bold;">{{$orderId}}</span>
        <table style="border-collapse: collapse;  border: 1px solid #ccc; width: 98%; margin-top: 15px;" border="1" bordercolor="#ccc">
            <thead>
            <tr style="background-color: lightgray;">
                <th style="padding:8px; text-align: center; border-bottom: 1px solid #ddd;">#</th>
                <th style="padding:8px; text-align: center; border-bottom: 1px solid #ddd;">Product Name</th>
                <th style="padding:8px; text-align: center; border-bottom: 1px solid #ddd;">Color</th>
                <th style="padding:8px; text-align: left; border-bottom: 1px solid #ddd; text-align: center;" width="100">Size</th>
                <th style="padding:8px; text-align: left; border-bottom: 1px solid #ddd; text-align: center;" width="100">Qty</th>
            </tr>
            </thead>
            <tbody>
            <?php $total=0; ?>
            @foreach($products as $index=>$product)

                <tr>
                    <td style="padding:8px; text-align: center; border-bottom: 1px solid #ddd;">{{$index+1}}</td>
                    <td style="padding:8px; text-align: left; border-bottom: 1px solid #ddd;">{{$product->proName}}</td>
                    <td style="padding:8px; text-align: center; border-bottom: 1px solid #ddd;">{{isset($product->color)?$product->color:''}}</td>
                    <td style="padding:8px; border-bottom: 1px solid #ddd; text-align: center;" width="100">{{isset($product->size)?$product->size:''}}</td>
                    <td style="padding:8px; border-bottom: 1px solid #ddd; text-align: center;" width="100">{{$product->qty}}</td>
                </tr>
               <?php  // $total += $product->subPrice;?>
            @endforeach


{{--            <tr>--}}
{{--                <td colspan="3" style="padding:8px; text-align: center; border-bottom: 1px solid #ddd;">Total</td>--}}
{{--                <td colspan="4" style="text-align: center; border-bottom: 1px solid #ddd; font-weight: bold; color: #1cbac8">$ {{$total}}</td>--}}
{{--            </tr>--}}
            </tbody>
            <tfoot>

            </tfoot>
        </table>
        <hr style="margin-top: 40px; margin-right: 10px; border: 0; border-top: 1px solid #ccc;">
        <p style="padding-bottom: 10px; line-height: 25px;">Please do not reply to this email. Emails sent to this address will not be answered.<br>
            <b>Our contact +855 23 99 61 72 | information@ione2u.com</b><br>
            No. 25, Mao Tse Tung Blvd, 1st Floor, iOne Building, Boeung Keng Kang I,<br> Chamkar Morn, Phnom Penh, Cambodia.
        </p>
    </div>
</div>
