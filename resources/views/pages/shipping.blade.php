
@extends('partials.layout')
@section('title', 'Shipping')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h5>Billing Details</h5>
                <div class="row">
                    <div class="col-md-6 pd-bottom">
                        <label>First Name: </label>
                        <i style="color: red; font-size: 11px;" class="font-weight-light" hidden id="firstNameError">First name required!</i>
                        <input class="text no-border-focus pl-lg-2" type="text" name="firstName" id="firstName">
                    </div>

                    <div class="col-md-6 pd-bottom">
                        <label>Last Name: </label>
                        <i style="color: red; font-size: 11px;" class="font-weight-light" hidden id="lastNameError">Last name required!</i>
                        <input class="text no-border-focus pl-lg-2" type="text" name="lastName" id="lastName">
                    </div>

                    <div class="col-md-12 pd-bottom">
                        <label>Phone Number: </label>
                        <i style="color: red; font-size: 11px;" class="font-weight-light" hidden id="phoneError">Phone number required!</i>
                        <i style="color: red; font-size: 11px;" class="font-weight-light" hidden id="phoneError1">Invalid phone number!</i>
{{--                        <input class="text no-border-focus pl-lg-2" type="text" name="phoneNum" id="phoneNum" maxlength="10">--}}
                        <div class="input-group input-group-sm text">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-sm">+855</span>
                            </div>
                            <input type="text" class="no-border-focus" id="phoneNum" placeholder="12345678" maxlength="10" style="width: 90%; border: 1px solid #dedede;">
                        </div>
                    </div>

                    <div class="col-md-12 pd-bottom">
                        <label>Email: </label>
                        <i style="color: red; font-size: 11px;" class="font-weight-light" hidden id="emailError">Email address required!</i>
                        <i style="color: red; font-size: 11px;" class="font-weight-light" hidden id="emailError1">Invalid email address!</i>
                        <input class="text no-border-focus pl-lg-2" type="text" name="email" id="email">
                    </div>
                    <div class="col-md-12 pd-bottom">
                        <div style="border: solid 1px #ccc; margin-top: 10px; padding-bottom: 10px;">
                            <div style="background-color: whitesmoke; padding-top: 10px;">
                                <p class="text-center" style="padding-bottom: 10px;">
                                    <i class="fa fa-home " style="font-size: 30px;"></i><br>
                                    Store Pickup
                                </p>
                            </div>
                            <div class="col-md-12">
                                <p>Please choose a pickup location:</p>
                            </div>
                            <div class="col-md-12" id="locationpickup">
                                <div class="active-pickup-location default-pickup-location" id="ione-canadia">
                                    <i class="fa fa-check-circle-o" style="font-size: 20px; color: #1cbac8;"></i><span style="padding-left: 10px; font-weight: bold;">iOne Canadia</span>
                                    <p class="font-weight-light" style="font-size: 13px;">Canadia Tower, Ground Floor,
                                        #315, Monivong Blvd. & St. 110,
                                        Daun Penh, Chamkar Morn,
                                        Phnom Penh, Cambodia.<br>
                                        <b style="font-size: 15px;">023 99 88 89</b>
                                    </p>
                                </div>
                                <div class="default-pickup-location" id="ione-ratana">
                                    <i class="fa fa-circle-o" style="font-size: 20px;"></i><span style="padding-left: 10px; font-weight: bold;">iOne Ratana</span>
                                    <p class="font-weight-light" style="font-size: 13px;">Ratana Plaza, Ground Floor,
                                        Chong Tnol Khang Keut, Russian Confederation Blvd.,
                                        Teuk Thlar, Khan Sen Sok,
                                        Phnom Penh, Cambodia.<br>
                                        <b style="font-size: 15px;">023 88 31 01</b>
                                    </p>
                                </div>
                                <div class="default-pickup-location" id="ione-sensok">
                                    <i class="fa fa-circle-o" style="font-size: 20px;"></i><span style="padding-left: 10px; font-weight: bold;">iOne Sen Sok</span>
                                    <p class="font-weight-light" style="font-size: 13px;">1st Floor, AEON Mall Sen Sok City, St. 1003, Sangkat Phnom Penh Thmey, Khan Sen Sok, 12101 Phnom Penh.<br>
                                        <b style="font-size: 15px;">023 99 31 61</b>
                                    </p>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6 pd-bottom">
                <h5>Additional Information</h5>
                <div class="row">
                    <div class="col-md-12 pd-bottom">
                        <label>Order Notes </label>
                        <textarea cols="120" rows="9" style="max-width:100%; border: solid 1px #ccc;" class="no-border-focus pl-lg-2" placeholder="Notes about your order, e.g. special notes for delivery." id="orderNote"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <h5 class="your-order">Your Order</h5>
        <div>
            <table class="table">
                <thead>
                <tr style="background-color: whitesmoke;">
                    <td><strong> PRODUCT </strong></td>
                    <td class="text-center" width="300"><strong>QTY</strong></td>
                </tr>
                </thead>
                <tbody id="cart-table-body">

                </tbody>
            </table>
        </div>
{{--        <div>--}}
{{--            <table class="table">--}}
{{--                <thead>--}}
{{--                <tr style="border-bottom: 1px solid #dcdcde;">--}}
{{--                    <td style="background-color: whitesmoke;"><strong> SUBTOTAL </strong></td>--}}
{{--                    <td class="text-center" width="300" id="subTotalPrice">$ 0.00</td>--}}
{{--                </tr>--}}
{{--                <tr style="border-bottom: 1px solid #dcdcde;">--}}
{{--                    <td style="background-color: whitesmoke;"><strong> TOTAL </strong></td>--}}
{{--                    <td class="text-center" width="300" id="totalPrice">$ 0.00</td>--}}
{{--                </tr>--}}
{{--                </thead>--}}
{{--            </table>--}}
{{--        </div>--}}
        <div  style="background-color: whitesmoke;margin-top:30px; padding-top: 10px;">
            <div class="col-md-12">
                <h6> Payment Method </h6>
                <label>Pay securely through any payment type</label>
            </div>
            <form style="padding-left: 50px; padding-bottom: 10px;">
                <input type="radio" name="payway" value="payStore" id="payStore" checked> <label for="payStore"> Pay at Store</label><br>
{{--                <input type="radio" name="payway" value="payABA" id="payABA"> <label for="payABA">Pay with ABA Payway</label><br>--}}
            </form>
        </div>
        <div class="row" style="padding-top: 50px;">
            <div class="col-md-12" style="text-align: right;">
                <form method="post" action="{{url('sendmail')}}" >
                    @csrf
                    <input type="hidden" name="firstName">
                    <input type="hidden" name="lastName">
                    <input type="hidden" name="email">
                    <input type="hidden" name="phone">
                    <input type="hidden" name="pickup">
                    <input type="hidden" name="payway">
                    <input type="hidden" name="orderNote">
                    <textarea hidden name="product"></textarea>
                    <button type="submit" id="hiddenButton" hidden></button>
                </form>
                <button type="button" class="btn" id="btn-place-order" style="background-color: #1cbac8; color: white;" >PLACE ORDER</button>

            </div>
        </div>

    </div>
@endsection

@section('cart-api')
    <script>
        var pickupLocaltion = 'iOne Canadia';

        $('#phoneNum').bind('keyup paste', function(){
            this.value = this.value.replace(/[^0-9]/g, '');
        });

        $('#ione-canadia').click(function () {
            $('#locationpickup').find('.active-pickup-location').removeClass('active-pickup-location');
            let circleIcon = $('#locationpickup').find('i');
            circleIcon.removeClass('fa-check-circle-o');
            circleIcon.addClass('fa-circle-o');
            circleIcon.css('color', 'black');


            $(this).addClass('active-pickup-location');
            $(this).find('i').removeClass('fa-circle-o');
            $(this).find('i').addClass('fa-check-circle-o');
            $(this).find('i').css('color', '#1cbac8');

            pickupLocaltion = 'iOne Canadia';
        });

        $('#ione-ratana').click(function () {
            $('#locationpickup').find('.active-pickup-location').removeClass('active-pickup-location');
            let circleIcon = $('#locationpickup').find('i');
            circleIcon.removeClass('fa-check-circle-o');
            circleIcon.addClass('fa-circle-o');
            circleIcon.css('color', 'black');


            $(this).addClass('active-pickup-location');
            $(this).find('i').removeClass('fa-circle-o');
            $(this).find('i').addClass('fa-check-circle-o');
            $(this).find('i').css('color', '#1cbac8');

            pickupLocaltion = 'iOne Ratana';
        });

        $('#ione-sensok').click(function () {
            $('#locationpickup').find('.active-pickup-location').removeClass('active-pickup-location');
            let circleIcon = $('#locationpickup').find('i');
            circleIcon.removeClass('fa-check-circle-o');
            circleIcon.addClass('fa-circle-o');
            circleIcon.css('color', 'black');


            $(this).addClass('active-pickup-location');
            $(this).find('i').removeClass('fa-circle-o');
            $(this).find('i').addClass('fa-check-circle-o');
            $(this).find('i').css('color', '#1cbac8');

            pickupLocaltion = 'iOne Sen Sok';
        });




        let userCartCount = JSON.parse(localStorage.getItem('userCart'));

        if(userCartCount != null) {
            let usd = '$ ';
            let totalPrice = 0;
            $.each(userCartCount, function (index, item) {
                let cartHtml = '<tr>' +
                    '               <td>'+item.proName+'</td><td class="text-center">1</td>' +
                    // '               <td class="text-center" width="300">'+usd+item.subPrice.toFixed(2)+'</td>' +
                    '            </tr>';

                $('#cart-table-body').append(cartHtml);
                totalPrice += item.subPrice;
            });
            $('#subTotalPrice').text('$ ' + totalPrice.toFixed(2));
            $('#totalPrice').text('$ ' + totalPrice.toFixed(2));






            $('body').on('click', '#btn-place-order', function () {

                var firstName = $('#firstName').val();
                var lastName = $('#lastName').val();
                var email = $('#email').val();
                var phone = $('#phoneNum').val();
                if (firstName == '') {
                    $('#firstName').css('border', '2px solid red');
                    $('#lastName').css('border', '1px solid #ccc');
                    $('#email').css('border', '1px solid #ccc');
                    $('#phoneNum').css('border', '1px solid #ccc');

                    $('#firstNameError').removeAttr('hidden');
                    $('#alert-missing-fill').css('display', 'block');
                    setTimeout(function () {
                        $('#alert-missing-fill').css('display', 'none');
                    }, 2000);
                } else if (lastName == '') {
                    $('#lastName').css('border', '2px solid red');
                    $('#firstName').css('border', '1px solid #ccc');
                    $('#email').css('border', '1px solid #ccc');
                    $('#phoneNum').css('border', '1px solid #ccc');

                    $('#lastNameError').removeAttr('hidden');
                    $('#alert-missing-fill').css('display', 'block');
                    setTimeout(function () {
                        $('#alert-missing-fill').css('display', 'none');
                    }, 2000);
                } else if (email == ''){
                    $('#email').css('border', '2px solid red');
                    $('#lastName').css('border', '1px solid #ccc');
                    $('#firstName').css('border', '1px solid #ccc');
                    $('#phoneNum').css('border', '1px solid #ccc');

                    $('#emailError').removeAttr('hidden');
                    $('#alert-missing-fill').css('display', 'block');
                    setTimeout(function () {
                        $('#alert-missing-fill').css('display', 'none');
                    }, 2000);
                } else if (!validateEmail(email)) {
                    $('#email').css('border', '2px solid red');
                    $('#lastName').css('border', '1px solid #ccc');
                    $('#firstName').css('border', '1px solid #ccc');
                    $('#phoneNum').css('border', '1px solid #ccc');


                    $('#emailError1').removeAttr('hidden');
                    $('#alert-missing-fill').css('display', 'block');
                    setTimeout(function () {
                        $('#alert-missing-fill').css('display', 'none');
                    }, 2000);
                } else if (phone == '') {
                    $('#phoneNum').css('border', '2px solid red');
                    $('#lastName').css('border', '1px solid #ccc');
                    $('#email').css('border', '1px solid #ccc');
                    $('#firstName').css('border', '1px solid #ccc');

                    $('#phoneError').removeAttr('hidden');
                    $('#alert-missing-fill').css('display', 'block');
                    setTimeout(function () {
                        $('#alert-missing-fill').css('display', 'none');
                    }, 2000);
                } else if (phone.length < 8) {
                    $('#phoneNum').css('border', '2px solid red');
                    $('#lastName').css('border', '1px solid #ccc');
                    $('#email').css('border', '1px solid #ccc');
                    $('#firstName').css('border', '1px solid #ccc');

                    $('#phoneError1').removeAttr('hidden');
                    $('#alert-missing-fill').css('display', 'block');
                    setTimeout(function () {
                        $('#alert-missing-fill').css('display', 'none');
                    }, 2000);
                } else {
                    $('form').find("input[name='firstName']").val(firstName);
                    $('form').find("input[name='lastName']").val(lastName);
                    $('form').find("input[name='phone']").val(phone);
                    $('form').find("input[name='email']").val(email);
                    $('form').find("input[name='pickup']").val(pickupLocaltion);
                    $('form').find("input[name='payway']").val('Pay at store');
                    $('form').find("input[name='orderNote']").val($('#orderNote').val());
                    $('form').find("textarea[name='product']").val(JSON.stringify(userCartCount));

                    $('#hiddenButton').click();
                }




                $('#firstName').focus(function () {
                    $(this).css('border', '1px solid #ccc');
                    $('#firstNameError').attr('hidden', 'true');
                });
                $('#lastName').focus(function () {
                    $(this).css('border', '1px solid #ccc');
                    $('#lastNameError').attr('hidden', 'true');
                });
                $('#email').focus(function () {
                    $(this).css('border', '1px solid #ccc');
                    $('#emailError').attr('hidden', 'true');
                    $('#emailError1').attr('hidden', 'true');
                });
                $('#phoneNum').focus(function () {
                    $(this).css('border', '1px solid #ccc');
                    $('#phoneError').attr('hidden', 'true');
                    $('#phoneError1').attr('hidden', 'true');
                });


                function validateEmail(email) {
                    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(String(email).toLowerCase());
                }

            });


        }

    </script>
@endsection

