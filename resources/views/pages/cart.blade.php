@extends('partials.layout')
@section('title', 'Cart')

@section('content')

    <table  id="customers">
        <thead>
            <tr>
                <th style="text-align: center;">PRODUCT</th>
                <th>PRICE</th>
                <th>QUANTITY</th>
                <th class="text-center">TOTAL</th>
            </tr>
        </thead>
        <tbody class="table-body">

        </tbody>
        <tr style="background-color: whitesmoke;">
            <td></td>
            <td></td>
            <td></td>
            <td>
                <button type="button" class="btn" id="btn-update-cart" style="background-color: #1cbac8; color: white;" >UPDATE CART</button>
            </td>
        </tr>
    </table>

    <div class=" mb-lg-5 mt-lg-5">
        <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <p>Cart Totals</p>
                <table class="table">
                    <tr>
                        <td style="background-color: whitesmoke;">Subtotal</td>
                        <td style="background-color: #f9f9f9;" class="text-center" id="sub-total-price">$1992</td>
                    </tr>
                    <tr style="border-bottom: 1px solid #dcdcde;">
                        <td style="background-color: whitesmoke;">Total</td>
                        <td style="background-color: #f9f9f9;" class="text-center" id="total-price">$1992</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="d-flex flex-row-reverse mt-lg-4">
            <button type="button" class="btn" style="background-color: #1cbac8; color: white;" id="btn-proceed-to-checkout">PROCEED TO CHECKOUT</button>
        </div>
    </div>


@endsection

@section('cart-api')
    <script>
        //localStorage.setItem('isBadgeVal', true);



        //localStorage.removeItem('isBadgeVal');
        // let isBadgeVal = localStorage.getItem('isBadgeVal');
        //
        // if(isBadgeVal == null) {
        //     window.location.reload(history.back());
        // }



        //let userId = localStorage.getItem('userId');
        let cartRemove = [];
        let isUpdateCart = true;
        if(userId != null) {
            let cartData = {userId: userId}
            $.ajax({
                type: 'POST',
                url: apiUrl('my_cart'),
                data: cartData,
                headers: {
                    'X-Parse-Application-Id': apiAppId()s
                },
                async: false
            }).done(function (res) {
                let result = res.result;
                console.log(result);

                $.each(result, function (index, item) {
                    let usd = '$ ';
                    let tbody = '<tr class="cart-table-row" cart-id="'+item.objectId+'"  cart-color="'+item.color+'" cart-size="'+item.size+'"  id="cart-table-row'+index+'">' +
                        '                <td>' +
                        '                    <i class="fa fa-times remove-cart-icon" aria-hidden="true" style=""></i>' +
                        '                    <img src="'+item.product.images[0]+'" style="height: 200px; width:200px; padding-left: 10px;">' +
                        '                    <label style="color: #1cbac8;padding-left: 20px; width: 390px; cursor: pointer;" class="product-name">'+item.product.name+'</label>' +
                        '                    <input type="hidden" value="'+item.product.objectId+'" class="product-id"/>'+
                        '                </td>' +
                        '                <td class="pro-price">'+usd+item.product.price.toFixed(2)+'</td>' +
                        '                <td>' +
                        '                    <div class="cart-row">' +
                        '                        <div class="btn-group" style="height: 38px;">' +
                        '                        <span class="input-group">' +
                        '                            <button type="button" class="btn-cart-qty btn-minus" id="btn-minus" style="border-bottom-left-radius: 5px; border-top-left-radius: 5px;"><i class="fa fa-minus"></i></button>' +
                        '                        </span>' +
                        '                            <input style="width: 50px;" value="'+item.qty+'" disabled class="text-center qty-val" id="qty">' +
                        '                            <input value="'+item.product.available+'" type="hidden" class="product-available"/>' +
                        '                            <span class="input-group">' +
                        '                            <button type="button" class="btn-cart-qty btn-plus" id="btn-plus" style="border-bottom-right-radius: 5px; border-top-right-radius: 5px;"><i class="fa fa-plus"></i></button>' +
                        '                        </span>' +
                        '                        </div>' +
                        '                    </div>' +
                        '                </td>' +
                        '                <td class="text-center pro-total-price">'+usd+(item.price).toFixed(2)+'</td>' +
                        '            </tr>';

                    $('.table-body').append(tbody);
                });


                $('body').on('click', '.remove-cart-icon', function () {
                    let parent = $(this).parent().parent();
                    parent.remove();
                    let qty = parent.find('.qty-val').val();
                    let cartCount = $('#cart-count');
                    let cartVal = cartCount.text();
                    let newCartCount = cartVal - qty;
                    cartCount.text(newCartCount);

                    // subtotal price
                    let proTotalPrice = parent.find('.pro-total-price').text();
                    proTotalPrice = proTotalPrice.substring(1);
                    proTotalPrice = parseFloat(proTotalPrice);
                    let subTotalPrice = $('#sub-total-price').text();
                    subTotalPrice = subTotalPrice.substring(1);
                    subTotalPrice = parseFloat(subTotalPrice);
                    let newSubtotalPrice = subTotalPrice - proTotalPrice;
                    $('#sub-total-price').text('$ '+newSubtotalPrice.toFixed(2));
                    $('#total-price').text('$ '+newSubtotalPrice.toFixed(2));



                    let cartIds = parent.attr('cart-id');
                    cartRemove.push(cartIds);
                });



                $('body').on('click', '.btn-minus', function () {
                    isUpdateCart = false;
                    let parent = $(this).parent().parent();
                    let tableRow = parent.parent().parent().parent();



                    let qty = parent.find('.qty-val').val();
                    qty --;
                    if(qty >= 1){
                        let cartCount = $('#cart-count');
                        let cartVal = cartCount.text();
                        let newCartCount = cartVal - 1;
                        cartCount.text(newCartCount);


                        // update total price
                        let price = tableRow.find('.pro-price').text();
                        price = price.substring(1);
                        price = parseFloat(price);

                        let totalPrice = tableRow.find('.pro-total-price').text();
                        totalPrice = totalPrice.substring(1);
                        totalPrice = parseFloat(totalPrice);

                        let newTotalPrice = totalPrice - price;
                        tableRow.find('.pro-total-price').text('$ ' +newTotalPrice.toFixed(2));

                        let subTotalPrice = $('#sub-total-price').text();
                        subTotalPrice = subTotalPrice.substring(1);
                        subTotalPrice = parseFloat(subTotalPrice);
                        let newSubtotalPrice = subTotalPrice - price;
                        $('#sub-total-price').text('$ '+newSubtotalPrice.toFixed(2));
                        $('#total-price').text('$ '+newSubtotalPrice.toFixed(2));
                    }
                    if(qty < 1) {
                        qty = 1;
                    }
                    parent.find('.qty-val').val(qty);
                });


                $('body').on('click', '.btn-plus', function () {
                    isUpdateCart = false;
                    let parent = $(this).parent().parent();
                    let tableRow = parent.parent().parent().parent();
                    let proAvailable = parent.find('.product-available').val();
                    let qty = parent.find('.qty-val').val();
                    qty ++;



                    if(qty <= proAvailable) {
                        let cartCount = $('#cart-count');
                        let cartVal = parseInt(cartCount.text());
                        let newCartCount = cartVal + 1;
                        cartCount.text(newCartCount);


                        // update total price
                        let price = tableRow.find('.pro-price').text();
                        price = price.substring(1);
                        price = parseFloat(price);

                        let totalPrice = tableRow.find('.pro-total-price').text();
                        totalPrice = totalPrice.substring(1);
                        totalPrice = parseFloat(totalPrice);

                        let newTotalPrice = totalPrice + price;
                        tableRow.find('.pro-total-price').text('$ ' +newTotalPrice.toFixed(2));

                        let subTotalPrice = $('#sub-total-price').text();
                        subTotalPrice = subTotalPrice.substring(1);
                        subTotalPrice = parseFloat(subTotalPrice);
                        let newSubtotalPrice = subTotalPrice + price;
                        $('#sub-total-price').text('$ '+newSubtotalPrice.toFixed(2));
                        $('#total-price').text('$ '+newSubtotalPrice.toFixed(2));

                    }

                    if(qty > proAvailable){
                        qty = proAvailable;
                        if(proAvailable > 1) {
                            Swal.fire({
                                title: 'Invalid!',
                                text: 'This product available only '+proAvailable+ ' items',
                                type: 'warning',
                                confirmButtonText: 'Ok',
                                confirmButtonColor: '#1cbac8',
                            })
                        } else {
                            Swal.fire({
                                title: 'Invalid!',
                                text: 'This product available only 1 item',
                                type: 'warning',
                                confirmButtonText: 'Ok',
                                confirmButtonColor: '#1cbac8',
                            })
                        }

                    }
                    parent.find('.qty-val').val(qty);


                });

            });


            $('body').on('click', '.product-name', function () {
                let parent = $(this).parent();
                let productId = parent.find('.product-id').val();
                //console.log(productId);
                location.href="{{url('product-detail?productId=')}}"+productId;
            });



            //total price
            let totalPrice1 = 0;
            let tableBody = $('.table-body').find('.cart-table-row')
            for (let i = 0; i< tableBody.length; i ++){
                let totalPrice = $('#cart-table-row'+i).find('.pro-total-price').text();
                totalPrice = totalPrice.substring(1);
                totalPrice1 += parseFloat(totalPrice);
            }
            $('#sub-total-price').text('$ '+totalPrice1);
            $('#total-price').text('$ '+totalPrice1);



            var cartToUpdate = [];
            $('body').on('click', '#btn-update-cart', function () {
                cartToUpdate = [];
                let tableBody = $('.table-body').find('.cart-table-row');
                for (let i = 0; i< tableBody.length; i ++){
                    let cartId = $('#cart-table-row'+i).attr('cart-id');
                    let cartQty = $('#cart-table-row'+i).find('.qty-val').val();
                    cartQty = parseInt(cartQty);
                    let cartPrice = $('#cart-table-row'+i).find('.pro-total-price').text();
                    cartPrice = cartPrice.substring(1);
                    cartPrice = parseFloat(cartPrice);
                    let cartColor = $('#cart-table-row'+i).attr('cart-color');
                    let cartSize = $('#cart-table-row'+i).attr('cart-size');

                    let cartData = {
                        cartId: cartId,
                        cartQty: cartQty,
                        cartPrice: cartPrice,
                        cartColor: cartColor,
                        cartSize: cartSize
                    }
                    cartToUpdate.push(cartData);
                }


                if(cartToUpdate.length > 0) {
                    $.each(cartToUpdate, function (index, item) {
                        // let cartRequest = {
                        //     cartId: item.cartId,
                        //     qty: item.cartQty,
                        //     price: item.cartPrice,
                        //     size: item.cartSize,
                        //     color: item.cartColor
                        // }
                        //console.log(item.cartPrice);
                        $.ajax({
                            type: 'POST',
                            url: apiUrl('shopping_web_update_cart'),
                            data: "{\"cartId\": \""+item.cartId+"\",  \"size\": \""+item.cartSize+"\",  \"color\": \""+item.cartColor+"\", \"qty\": "+item.cartQty+", \"price\": "+item.cartPrice+"}",
                            headers: {
                                'X-Parse-Application-Id': apiAppId()
                            },
                            contentType: 'application/json',
                            async: false
                        }).done(function (res) {
                            console.log(res);

                            if (index+1 === cartToUpdate.length) {
                                Swal.fire({
                                    title: 'Success!',
                                    text: 'Your cart has been updated.',
                                    type: 'success',
                                    confirmButtonText: 'Ok',
                                    confirmButtonColor: '#1cbac8',
                                })
                            }
                        });

                    });
                }


                if(cartRemove.length > 0) {
                    for(let i = 0; i < cartRemove.length; i ++) {
                        console.log(cartRemove[i]);
                        let request = {
                            cartId: cartRemove[i],
                            qty: 0
                        }
                        $.ajax({
                            type: 'POST',
                            url: apiUrl('update_cart'),
                            data: request,
                            headers: {
                                'X-Parse-Application-Id': apiAppId()
                            },
                            async: false
                        }).done(function (res) {
                            console.log(res);
                        });
                    }
                }

                $.ajax({

                    type: 'POST',
                    url: apiUrl('my_cart'),
                    data: {userId:userId},
                    headers: {
                        'X-Parse-Application-Id': apiAppId()
                    },
                    async: false
                }).done(function (res) {
                    let result = res.result;
                    if(result.length < 1) {
                        localStorage.removeItem('isBadgeVal');
                        location.reload();
                    }
                    //console.log(res);
                });


            });

            $('body').on('click', '#btn-proceed-to-checkout', function () {
                location.href="{{url('/shipping')}}";
            });


        }

        // $(window).bind("beforeunload",function(event) {
        //     return "You have some unsaved changes";
        // });




    </script>
@endsection
