<div class="row">
    <div class="col-md-12 footer-style">
        <div class="container">
            <div class="row" style="color: #888888">
                <div class="col-md-4" style="padding-top: 50px;">
                    <h4 style="    font: 13px Montserrat, Arial, Helvetica, sans-serif; color: #ffffff">About</h4>
                    <p style="padding-top: 20px; font-size: 13px;">
                        iOne is the leading Apple retailer in the Kingdom of Cambodia with an extensive network of stores including three Apple Premium Reseller (APR) stores and one Apple Authorised Service Provider (AASP) covering Phnom Penh.<br>
                        Our iOne stores offer the official Apple iPhone 7, iPhone 7 Plus, iPhone 6s, iPhone 6s Plus, iPad Pro, MacBook, MacBook Air, MacBook Pro with Retina display, iMac with Retina 5K display, Mac Pro, Mac mini, iPod Touch, iPod nano, iPod shuffle and a full range of Apple accessories.
                    </p>
                </div>
                <div class="col-md-4" style="padding-top: 50px;">
                    <h4 style="font: 13px Montserrat, Arial, Helvetica, sans-serif;color: #ffffff;">iOne Service Center</h4>
                    <p style="padding-top: 20px; font-size: 13px; color: #888888">
                        <i class="fa fa-map-marker" style="padding-right: 10px;"></i>
                        2rd Floor, Unit 01, No. 25, Mao Tse Tung Blvd,
                        Boeung Keng Kang I, Chamkar Morn, Phnom Penh, Cambodia
                    <p> <i class="fa fa-phone" style="padding-right: 10px;font-size: 15px;"></i>023 99 75 75 | 081 99 75 75 | 085 99 75 75</p>
                    <p><i class="fa fa-globe" style="padding-right: 10px; "></i><a href="http://www.ioneservice.com" target="_blank" style="padding-right: 10px; color: gray; font-size: 13px; ">www.ioneservice.com</a></p>
                </div>
                <div class="col-md-4" style="padding-top: 50px;">
                    <iframe name="f30cda31c18ccc" width="360px" height="300px" title="fb:like_box Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://web.facebook.com/plugins/like_box.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D44%23cb%3Df2bf90c321a4a08%26domain%3Dwww.ione.com.kh%26origin%3Dhttps%253A%252F%252Fwww.ione.com.kh%252Ff1bb33dc6e4f984%26relation%3Dparent.parent&amp;container_width=280&amp;header=true&amp;height=300&amp;href=https%3A%2F%2Fwww.facebook.com%2FiOneAppleAuthorizedReseller&amp;locale=en_US&amp;sdk=joey&amp;show_faces=false&amp;stream=true&amp;width=360" style="border: none; visibility: visible; width: 360px; height: 300px;" class=""></iframe>
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 30px;">
            <div class="col-md-12" style="background-color: #1b1b1b; height: 100%">
                <div class="container">
                    <div class="row" style="color: #999999; font-size: 13px;padding-top: 15px; padding-bottom: 10px;">
                        <div class="col-md-8">
                            <div class="copyright-text copyright-col1">
                                Copyright © 2006-2018 <a href="http://www.ione.com.kh" style="color: whitesmoke">iOne Co., Ltd.</a>, Apple Premium Reseller in Cambodia. All Rights Reserved.
                            </div>
                        </div>
                        <div class="col-md-4" style="text-align: right">
                            <div class="copyright-col2">
                                <div class="social-icons clearfix" >
                                    <a href="mailto://information@ione2u.com" target="_blank" title="Dribbble"><i class="fa fa-envelope" style="font-size: 20px; padding-right: 20px; color: #999999"></i></a>
                                    <a href="https://www.facebook.com/iOneApplePremiumReseller" target="_blank" title="Facebook"><i class="fa fa-facebook" style="font-size: 20px; padding-right: 20px; color: #999999"></i></a>
                                    <a href="http://instagram.com/iOneCambodia" target="_blank" title="Instagram"><i class="fa fa-instagram" style="font-size: 20px; padding-right: 20px; color: #999999"></i></a>
                                    <a href="http://youtube.com/iOneCambodia" target="_blank" title="YouTube"><i class="fa fa-youtube-play" style="font-size: 20px; color: #999999"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <a id="back-to-top" href="#" class=" btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><i class="fa fa-chevron-up" style="font-size: 13px; bottom: 10px; position: absolute; right: 15px;"></i></a>

    </div>
</div>
