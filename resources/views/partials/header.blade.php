<nav class="navbar navbar-expand-lg navbar-light  sticky-top addnavbarshadow navbar-height">
    <div class="container">
        <input class="form-control" placeholder="To Search start typing..." id="search-input" type="hidden" style="font-family: inherit;">
        <a class="navbar-brand" href="https://www.ione.com.kh" id="navbar-brand">
            <img src="{{url('images/ionelogo.png')}}" class="img-fluid">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent" >
            <ul class="navbar-nav ml-auto" id="menu-header">
                <li class="nav-item mydropdown">
                    <a class="navbar-text-style @yield('active-product-page')" href="{{url('/')}}">Prebooking</a>
                </li>
                <li class="nav-item mydropdown">
                    <a class="navbar-text-style" href="https://www.ione.com.kh/about_ione/" id="about-ione-menu">About iOne</a>
                    <div class="dropdown-content" id="about-ione-menu-item">
                        <a href="https://www.ione.com.kh/about_ione/" class="dropdown-content-item">About Us</a>
                        <a href="https://www.ione.com.kh/outlets-2/" class="dropdown-content-item">Outlets</a>
                        <a href="https://www.ione.com.kh/career/" class="dropdown-content-item">Careers</a>
                        <a href="https://www.ione.com.kh/contact_us/" class="dropdown-content-item">Contact Us</a>
                    </div>
                </li>
                <li class="nav-item mydropdown">
                    <a class="navbar-text-style" href="https://www.ione.com.kh/apple_products/">Learn More</a>
                    <div class="dropdown-content" id="about-ione-menu-item">
                        <a href="https://www.ione.com.kh/apple_products/" class="dropdown-content-item">Apple Products</a>
                        <a href="https://www.ione.com.kh/accessories-2/" class="dropdown-content-item">Accessories</a>
                    </div>
                </li>
                <li class="nav-item mydropdown">
                    <a class="navbar-text-style" href="#">Features</a>
                    <div class="dropdown-content" id="about-ione-menu-item">
                        <a href="https://www.ione.com.kh/easy_payment-2/" class="dropdown-content-item">Easy payment</a>
                        <a href="https://www.ione.com.kh/workshop/" class="dropdown-content-item">Workshop</a>
                    </div>
                </li>
                <li class="nav-item mydropdown">
                    <a class="navbar-text-style" href="https://www.ioneservice.com/">Support</a>
                </li>
                <li class="nav-item mydropdown">
                    <a class="navbar-text-style" href="http://ionecards.com/">iOne Cards</a>
                </li>
            </ul>
            <div class="navbar-vertical-line"></div>
            <div id="search-container">
                <i class="fa fa-search search-icon-header" id="search-icon" style="font-size:20px;" aria-hidden="true"></i>
                <i class="fa fa-times search-icon-header" id="remove-icon" style="font-size:20px;" aria-hidden="true"></i>
            </div>
            <div style="padding-left: 20px;">
                <i class="fa fa-shopping-cart cart-icon" aria-hidden="true" id="cart-icon" style="font-size: 20px; color: #c4c4c4;"></i>
                <span class="badge badge-danger custom-badge" id="cart-badge" style="display: none;"><span id="cart-count">0</span></span>
            </div>
{{--            <div style="padding-left: 40px;" class="dropdown">--}}
{{--                <i class="fa fa-user " aria-hidden="true" id="user-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px; color: #c4c4c4; display: none;"></i>--}}
{{--                <div class="dropdown-menu" aria-labelledby="user-icon" style="margin-top: 32px;">--}}
{{--                    <a class="dropdown-item" href="{{url('/myaccount')}}" style="color: #777777;"><i class="fa fa-cart-arrow-down"></i> My Order</a>--}}
{{--                    <a class="dropdown-item" href="#" style="color: #777777;" id="logout"><i class="fa fa-power-off"></i> Logout</a>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
</nav>
<script>
    $("#search-input").hide();
    $("#remove-icon").hide();
    $("#search-icon").click(function () {
        $(this).hide();
        $("#remove-icon").show();
        $("#search-input").show();
        $("#search-input").removeAttr('type');
        $(".navbar-vertical-line").hide();
        $("#menu-header").hide();
        $("#navbar-brand").hide();
    });
    $("#remove-icon").click(function () {
        $(this).hide();
        $("#search-input").hide();
        $("#search-icon").show();
        $(".navbar-vertical-line").show();
        $("#menu-header").show();
        $("#navbar-brand").show();
    });

    $("#search-input").focus(function () {

    });


    //cart, user-icon
    $('#cart-icon').on('click', function () {
        {{--userId = localStorage.getItem('userId');--}}
        {{--$.ajax({--}}
        {{--    type: 'POST',--}}
        {{--    url: apiUrl('my_cart'),--}}
        {{--    data: {userId: userId},--}}
        {{--    headers: {--}}
        {{--        'X-Parse-Application-Id': apiAppId()--}}
        {{--    },--}}
        {{--    async: false--}}
        {{--}).done(function(res){--}}
        {{--    let result = res.result;--}}
        {{--    if(result.length > 0) {--}}
        {{--        window.location.href="{{url('/cart')}}";--}}
        {{--    } else {--}}
        {{--        Swal.fire({--}}
        {{--            title: 'Empty cart!',--}}
        {{--            text: 'Your cart is empty.',--}}
        {{--            type: 'warning',--}}
        {{--            confirmButtonText: 'Ok',--}}
        {{--            confirmButtonColor: '#1cbac8',--}}
        {{--        })--}}
        {{--    }--}}
        {{--});--}}

        let totalItem = JSON.parse(localStorage.getItem('userCart'));
        if(totalItem != null) {
            window.location.href="{{url('/cart')}}";
        } else {
            Swal.fire({
                title: 'Empty cart!',
                text: 'Your cart is empty.',
                type: 'warning',
                confirmButtonText: 'Ok',
                confirmButtonColor: '#1cbac8',
            })
        }


    });
    $('#cart-badge, #user-icon').css('display', 'inline');
    // $('#cart-icon, #user-icon').hide();
    // $('#cart-badge, #user-icon').hide();
    // let userId = localStorage.getItem('userId');
    // if(userId != null) {
    //     $('#cart-icon, #user-icon').show();
    //     $('#cart-badge, #user-icon').show();
    //     $('#cart-badge, #user-icon').css('display', 'inline');
    // }





    $('#cart-icon, #user-icon').mouseover(function () {
        $(this).css({'color':'gray', 'cursor':'pointer'});
    });
    $('#cart-icon, #user-icon').mouseout(function () {
        $(this).css({'color':'#c4c4c4', 'cursor':'default'});
    });

    // cart count, badge value
    //getBadgeValue();
    // function getBadgeValue() {
    //     if(userId != null) {
    //         $.ajax({
    //             type: 'POST',
    //             url: apiUrl('my_cart'),
    //             data: {userId: userId},
    //             headers: {
    //                 'X-Parse-Application-Id': apiAppId()
    //             },
    //             async: false
    //         }).done(function(res){
    //             let result = res.result;
    //             let totalQty = 0;
    //             $.each(result, function (index, item) {
    //                 totalQty += item.qty;
    //             });
    //             $('#cart-count').text(totalQty);
    //
    //             if(result.length > 0) {
    //                 $('#cart-badge').show();
    //             } else {
    //                 $('#cart-badge').hide();
    //             }
    //         });
    //     }
    // }

    let totalItem = JSON.parse(localStorage.getItem('userCart'));
    var totalQty = 0;
    if(totalItem != null) {
        $('#cart-badge').show();
        $.each(totalItem, function (index, item) {
            totalQty += item.qty;
        });
        $('#cart-count').text(totalQty);
    }else {
        $('#cart-badge').hide();
    }




    // $('#user-icon').click(function () {
    //     $('nav').attr('style')?$('nav').removeAttr('style'):$('nav').css('overflow', 'hidden');
    //
    // });

    // $('.dropdown').on({
    //     "shown.bs.dropdown": function() { this.closable = false; },
    //     "click":             function() { this.closable = true; },
    //     "hide.bs.dropdown":  function() { return this.closable; }
    // });

    // $('.dropdown').on('hide.bs.dropdown', function () {
    //     $('nav').css('overflow', 'hidden');
    // });




    // $('#logout').on('click', function () {
    //     localStorage.clear();
    //     $('#cart-icon, #user-icon').hide();
    //     $('#cart-badge, #user-icon').hide();
    //     let userId = localStorage.getItem('userId');
    //     if(userId != null) {
    //         $('#cart-icon, #user-icon').show();
    //         $('#cart-badge, #user-icon').show();
    //         $('#cart-badge, #user-icon').css('display', 'inline');
    //     }
    //     location.href = "https://www.ione.com.kh/";
    // });


    detectWindowScreen();
    $(window).resize(function () {
        detectWindowScreen();
    });

    function detectWindowScreen() {
        if ($(window).width() < 960) {
            console.log('less than 960');
            $('#search-container').hide();
            $('#menu-header').css({'background-color':'black'});
            $('#menu-header').find('.mydropdown').removeClass('mydropdown');
        }
        else {
            console.log('more than 960');
            $('#search-container').show();
            $('#menu-header').css({'background-color':'transparent'});
            $('#menu-header').find('li').addClass('mydropdown');
        }
    }

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');




</script>
