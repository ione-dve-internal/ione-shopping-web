<!doctype html>
<html>
<head>
    <title>iOne | @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>


    <link rel="stylesheet"  type="text/css" href="{{url('css/custom-navbar.css')}}"/>
    <link rel="stylesheet" href="{{url('font-awesome-4.7.0/css/font-awesome.min.css')}}">

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

    <script src="{{url('js/jquery.min.js')}}"> </script>
    <script src="{{url('js/apiconnection.js')}}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="{{url('css/custom-pages.css')}}"/>


    @yield('pagination')

</head>
<body>

    <div class="top-info">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style=" color: whitesmoke; padding-top: 10px;">

                    <a href="mailto://information@ione2u.com" target="_blank" title="Dribbble" style="padding-right: 15px; margin-top: 10px;">
                        <i class="fa fa-envelope" style="color: gray; font-size: 20px; "></i>
                    </a>
                    <a href="https://www.facebook.com/iOneApplePremiumReseller" target="_blank" title="Facebook">
                        <i class="fa fa-facebook" style="color: gray; font-size: 20px; padding-right: 15px;"></i>
                    </a>
                    <a href="http://www.instagram.com/iOneCambodia" target="_blank" title="Instagram"><i class="fa fa-instagram" style="color: gray; font-size: 20px; padding-right: 15px;"></i></a>
                    <a href="http://www.youtube.com/iOneCambodia" target="_blank" title="YouTube"><i class="fa fa-youtube-play" style="color: gray; font-size: 20px; padding-right: 15px;"></i></a>
                </div>
                <div class="col-md-6 font-weight-light" style="text-align: right; padding-top: 10px; padding-bottom: 10px; font-size: 14px;">
                    iOne | Apple Premium Reseller in Cambodia
                </div>
            </div>
        </div>
    </div>
    @include('partials.header')
    <div class="container">
        <div id="alertContainer" class="fixed-top">

        </div>
        <div class="alert alert-success fixed-top text-center" role="alert" style="display: none; " id="alert-cart-update">
            Cart has been updated!
        </div>
        <div class="alert alert-danger fixed-top text-center" role="alert" style="display: none; " id="alert-missing-fill">
            Please check for missing fill in or error information.
        </div>
        @yield('content')

    </div>
    @include('partials.footer')
    @yield('product-api')
    @yield('cart-api')

</body>
</html>

