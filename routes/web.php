<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('partials.layout');
});
//Route::get('/', function() {
//    return view('partials.initialContent');
//});
Route::get('/', function() {
    return view('pages.product');
});
Route::get('/products', function (){
    return view('pages.product');
});
Route::get('/product-detail', function (){
    return view('pages.product-detail1');
});
Route::get('/myaccount', function (){
    return view('pages.user-account');
});


Route::post('sendmail', 'MailController@sendEmail');


/* testing */

Route::get('/shipping', function(){
    return view('pages.shipping');
});

Route::get('/success', function(){
    return view('pages.success-pay');
});

function test() {
    return "<script>var a = localStorage.getItem('userId');</script>";
}
Route::get('/cart', function(){
    return view('pages.cart1');
});

Route::get('/email', function () {
    return view('pages.mail1');
});


